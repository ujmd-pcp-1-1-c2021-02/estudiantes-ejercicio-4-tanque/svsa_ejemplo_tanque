﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tanque_H2O_PCP_1_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnEncenderUp_Click(object sender, EventArgs e)
        {
            btnEncenderUp.Visible = false;
            btnEncenderDown.Visible = true;
            btnApagarUp.Enabled = true;
            timerOn.Enabled = true;
            
         
        }

        private void timerOn_Tick(object sender, EventArgs e)
        {
            if (panelAgua.Height < panelWindow.Height)
            {
                panelAgua.Height = panelAgua.Height + 1;
                panelAgua.Top = panelAgua.Top - 1;
                numericUpDownAgua.Value++;
            }
            else
            {
                timerOn.Enabled = false;
            }
        }

        private void btnApagarUp_Click(object sender, EventArgs e)
        {
            btnApagarUp.Visible = false;
            btnApagarDown.Visible = true;
            btnEncenderDown.Visible = false;
            btnEncenderUp.Visible = true;
            timerOff.Enabled = true;
        }

        private void timerOff_Tick(object sender, EventArgs e)
        {
            if (panelAgua.Height > 0)
            {
                panelAgua.Height = panelAgua.Height - 1;
                panelAgua.Top = panelAgua.Top + 1;
                numericUpDownAgua.Value--;
            }
            else
            {
                timerOff.Enabled = false;
            }
        }

        private void numericUpDownAgua_ValueChanged(object sender, EventArgs e)
        {
            panelAgua.Height = (int)(numericUpDownAgua.Value - 0) * panelWindow.Height / (100 - 0);
        }
    }
}
