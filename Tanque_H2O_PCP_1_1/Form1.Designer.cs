﻿
namespace Tanque_H2O_PCP_1_1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panelWindow = new System.Windows.Forms.Panel();
            this.panelAgua = new System.Windows.Forms.Panel();
            this.btnApagarDown = new System.Windows.Forms.PictureBox();
            this.btnApagarUp = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnEncenderDown = new System.Windows.Forms.PictureBox();
            this.btnEncenderUp = new System.Windows.Forms.PictureBox();
            this.timerOn = new System.Windows.Forms.Timer(this.components);
            this.timerOff = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownAgua = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.panelWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnApagarDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnApagarUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEncenderDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEncenderUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAgua)).BeginInit();
            this.SuspendLayout();
            // 
            // panelWindow
            // 
            this.panelWindow.Controls.Add(this.panelAgua);
            this.panelWindow.Location = new System.Drawing.Point(404, 99);
            this.panelWindow.Name = "panelWindow";
            this.panelWindow.Size = new System.Drawing.Size(21, 200);
            this.panelWindow.TabIndex = 1;
            // 
            // panelAgua
            // 
            this.panelAgua.BackgroundImage = global::Tanque_H2O_PCP_1_1.Properties.Resources.ezgif_com_gif_maker__1_;
            this.panelAgua.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelAgua.Location = new System.Drawing.Point(0, 190);
            this.panelAgua.Name = "panelAgua";
            this.panelAgua.Size = new System.Drawing.Size(21, 10);
            this.panelAgua.TabIndex = 0;
            // 
            // btnApagarDown
            // 
            this.btnApagarDown.Image = global::Tanque_H2O_PCP_1_1.Properties.Resources.Fire_button__Red_Down_;
            this.btnApagarDown.Location = new System.Drawing.Point(123, 40);
            this.btnApagarDown.Name = "btnApagarDown";
            this.btnApagarDown.Size = new System.Drawing.Size(54, 52);
            this.btnApagarDown.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnApagarDown.TabIndex = 5;
            this.btnApagarDown.TabStop = false;
            // 
            // btnApagarUp
            // 
            this.btnApagarUp.Image = global::Tanque_H2O_PCP_1_1.Properties.Resources.Fire_button__Red_Up_;
            this.btnApagarUp.Location = new System.Drawing.Point(123, 40);
            this.btnApagarUp.Name = "btnApagarUp";
            this.btnApagarUp.Size = new System.Drawing.Size(54, 52);
            this.btnApagarUp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnApagarUp.TabIndex = 3;
            this.btnApagarUp.TabStop = false;
            this.btnApagarUp.Click += new System.EventHandler(this.btnApagarUp_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Tanque_H2O_PCP_1_1.Properties.Resources.GRP_ADcooler_Dcooler_1024x867;
            this.pictureBox1.Location = new System.Drawing.Point(29, -9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(536, 423);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnEncenderDown
            // 
            this.btnEncenderDown.Image = global::Tanque_H2O_PCP_1_1.Properties.Resources.Fire_button__Green_Down_;
            this.btnEncenderDown.Location = new System.Drawing.Point(29, 40);
            this.btnEncenderDown.Name = "btnEncenderDown";
            this.btnEncenderDown.Size = new System.Drawing.Size(54, 51);
            this.btnEncenderDown.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnEncenderDown.TabIndex = 4;
            this.btnEncenderDown.TabStop = false;
            // 
            // btnEncenderUp
            // 
            this.btnEncenderUp.Image = global::Tanque_H2O_PCP_1_1.Properties.Resources.Fire_button__Green_Up_;
            this.btnEncenderUp.Location = new System.Drawing.Point(29, 40);
            this.btnEncenderUp.Name = "btnEncenderUp";
            this.btnEncenderUp.Size = new System.Drawing.Size(54, 52);
            this.btnEncenderUp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnEncenderUp.TabIndex = 6;
            this.btnEncenderUp.TabStop = false;
            this.btnEncenderUp.Click += new System.EventHandler(this.btnEncenderUp_Click);
            // 
            // timerOn
            // 
            this.timerOn.Tick += new System.EventHandler(this.timerOn_Tick);
            // 
            // timerOff
            // 
            this.timerOff.Tick += new System.EventHandler(this.timerOff_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(175, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 21);
            this.label1.TabIndex = 7;
            this.label1.Text = "TANQUE DE AGUA";
            // 
            // numericUpDownAgua
            // 
            this.numericUpDownAgua.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownAgua.Location = new System.Drawing.Point(40, 152);
            this.numericUpDownAgua.Name = "numericUpDownAgua";
            this.numericUpDownAgua.ReadOnly = true;
            this.numericUpDownAgua.Size = new System.Drawing.Size(69, 25);
            this.numericUpDownAgua.TabIndex = 8;
            this.numericUpDownAgua.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownAgua.ValueChanged += new System.EventHandler(this.numericUpDownAgua_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(37, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 15);
            this.label2.TabIndex = 9;
            this.label2.Text = "Niven en Litros";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(528, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericUpDownAgua);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnEncenderUp);
            this.Controls.Add(this.btnApagarUp);
            this.Controls.Add(this.panelWindow);
            this.Controls.Add(this.btnEncenderDown);
            this.Controls.Add(this.btnApagarDown);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "TANQUE DE AGUA";
            this.panelWindow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnApagarDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnApagarUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEncenderDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEncenderUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAgua)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panelWindow;
        private System.Windows.Forms.Panel panelAgua;
        private System.Windows.Forms.PictureBox btnApagarUp;
        private System.Windows.Forms.PictureBox btnEncenderDown;
        private System.Windows.Forms.PictureBox btnApagarDown;
        private System.Windows.Forms.PictureBox btnEncenderUp;
        private System.Windows.Forms.Timer timerOn;
        private System.Windows.Forms.Timer timerOff;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDownAgua;
        private System.Windows.Forms.Label label2;
    }
}

